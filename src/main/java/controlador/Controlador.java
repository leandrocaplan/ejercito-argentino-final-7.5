package controlador;

import DAO.ServicioDAO;
import DAO.CompaniaDAO;
import DAO.CuartelDAO;
import DAO.ServicioRealizadoDAO;
import DAO.CuerpoDAO;
import DAO.MilitarDAO;
import DAO.HibernateUtil;
import DTO.MilitarDTO;
import java.io.File;
import java.util.List;
import org.apache.log4j.BasicConfigurator;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.AnnotationConfiguration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import javax.swing.JOptionPane;
import DTO.ServicioRealizadoDTO;
import DTO.SoldadoDTO;

@Controller
public class Controlador {

    private static final SessionFactory sessionFactory = buildSessionFactory();

    @Autowired
    private MilitarDAO militarDAO;

    @Autowired
    private CompaniaDAO companiaDAO;

    @Autowired
    private CuerpoDAO cuerpoDAO;

    @Autowired
    private CuartelDAO cuartelDAO;

    @Autowired
    private ServicioDAO servicioDAO;

    @Autowired
    private ServicioRealizadoDAO servicioRealizadoDAO;

    private MilitarDTO militarLogeado = null;

    private static SessionFactory buildSessionFactory() {
        try {
            // Create the SessionFactory from hibernate.cfg.xml
            return new AnnotationConfiguration().configure(new File("final.cgf.xml")).buildSessionFactory();

        } catch (Throwable ex) {
            // Make sure you log the exception, as it might be swallowed
            System.err.println("Initial SessionFactory creation failed." + ex);
            throw new ExceptionInInitializerError(ex);
        }
    }

    public static SessionFactory getSessionFactory() {
        return sessionFactory;
    }

    public static void shutdown() {
        // Close caches and connection pools
        getSessionFactory().close();
    }

    @GetMapping("/menuPrincipal")
    public String menuPrincipal(Model model) {
        if (this.militarLogeado == null) {
            return "index";
        }
        model.addAttribute("militar", this.militarLogeado);
        return "menuPrincipal";
    }

    @PostMapping("/login")
    public String login(@RequestParam(name = "codigo") String codigo, @RequestParam(name = "password") String password, Model model) {
        String pagina = "error";
        try {
            //JOptionPane.showMessageDialog(null, "Entro al controller");
            if (codigo.isEmpty() || password.isEmpty()) {
                String msgError = "Uno de los campos ingresados está vacio";
                model.addAttribute("error", msgError);
                return pagina;
            }
            this.militarLogeado = militarDAO.login(codigo);

            if (this.militarLogeado == null) {
                String msgError = "No se encontro el militar correspondiente";
                model.addAttribute("error", msgError);
                pagina = "error";
            } else if (!this.militarLogeado.getPassword().equals(password)) {
                String msgError = "La contraseña no es correcta";
                model.addAttribute("error", msgError);
                this.militarLogeado=null;
                pagina = "error";
            } else {
                //JOptionPane.showMessageDialog(null, "Entro al else del controller");
                //JOptionPane.showMessageDialog(null, militar.getNombre());
                //JOptionPane.showMessageDialog(null, militar.getApellido());
                //JOptionPane.showMessageDialog(null, militar.getGraduacion());
                model.addAttribute("militar", this.militarLogeado);

                pagina = "menuPrincipal";
            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e);
            model.addAttribute("error", e);
            pagina = "error";
        }
        return pagina;

    }

    @PostMapping("/opcionesMilitar")
    public String opcionesMilitar(@RequestParam(name = "opcion") String opcion, Model model) {

        String pagina;
        switch (opcion) {

            case "mostrar_militares":
                militarDAO.cargarMilitares();
                companiaDAO.cargarCompanias();
                cuerpoDAO.cargarCuerpos();
                cuartelDAO.cargarCuarteles();
                servicioDAO.cargarServicios();
                servicioRealizadoDAO.cargarServiciosRealizados();

                model.addAttribute("militares", militarDAO.getMilitares());
                model.addAttribute("companias", companiaDAO.getCompanias());
                model.addAttribute("cuerpos", cuerpoDAO.getCuerpos());
                model.addAttribute("cuarteles", cuartelDAO.getCuarteles());
                model.addAttribute("servicios", servicioDAO.getServicios());
                model.addAttribute("serviciosRealizados", servicioRealizadoDAO.getServiciosRealizados());
                pagina = "mostrarMilitares";
                break;

            case "mostrar_companias":

                companiaDAO.cargarCompanias();
                model.addAttribute("companias", companiaDAO.getCompanias());

                pagina = "mostrarCompanias";
                //vista.forward(request, response);
                break;

            case "mostrar_cuerpos":

                cuerpoDAO.cargarCuerpos();
                model.addAttribute("cuerpos", cuerpoDAO.getCuerpos());

                pagina = "mostrarCuerpos";
                break;

            case "mostrar_cuarteles":

                cuartelDAO.cargarCuarteles();
                model.addAttribute("cuarteles", cuartelDAO.getCuarteles());

                pagina = "mostrarCuarteles";
                break;

            case "mostrar_servicios":
                servicioDAO.cargarServicios();
                model.addAttribute("servicios", servicioDAO.getServicios());

                pagina = "mostrarServicios";

                break;

            case "alta_militar":

                pagina = "ingresarMilitar";
                break;
            case "alta_soldado":
                String faltante = "";
                boolean faltaElemento = false;

                companiaDAO.cargarCompanias();
                cuerpoDAO.cargarCuerpos();
                cuartelDAO.cargarCuarteles();

                if (companiaDAO.getCompanias().isEmpty()) {
                    faltaElemento = true;
                    faltante = "companias";
                } else if (cuerpoDAO.getCuerpos().isEmpty()) {
                    faltaElemento = true;
                    faltante = "cuerpos";
                } else if (cuartelDAO.getCuarteles().isEmpty()) {
                    faltaElemento = true;
                    faltante = "cuarteles";
                }
                if (!faltaElemento) {
                    model.addAttribute("companias", companiaDAO.getCompanias());
                    model.addAttribute("cuerpos", cuerpoDAO.getCuerpos());
                    model.addAttribute("cuarteles", cuartelDAO.getCuarteles());

                    pagina = "ingresarSoldado";
                } else {
                    model.addAttribute("error", "No se puede dar de alta un soldado, ya que no hay " + faltante + " disponibles");
                    pagina = "error";
                }
                break;
            case "alta_compania":
                pagina = "ingresarCompania";
                break;

            case "alta_cuerpo":
                pagina = "ingresarCuerpo";
                break;

            case "alta_cuartel":
                pagina = "ingresarCuartel";
                break;

            case "alta_servicio":
                pagina = "ingresarServicio";
                break;

            case "baja_militar":
                militarDAO.cargarMilitares();
                model.addAttribute("militares", militarDAO.getMilitares());
                pagina = "bajaMilitar";
                break;

            case "baja_soldado":
                militarDAO.cargarSoldados();

                model.addAttribute("militares", militarDAO.getSoldados());
                pagina = "bajaMilitar";
                break;
            case "baja_compania":
                companiaDAO.cargarCompanias();
                model.addAttribute("companias", companiaDAO.getCompanias());
                pagina = "bajaCompania";
                break;

            case "baja_cuerpo":
                cuerpoDAO.cargarCuerpos();
                model.addAttribute("cuerpos", cuerpoDAO.getCuerpos());
                pagina = "bajaCuerpo";
                break;
            case "baja_cuartel":
                cuartelDAO.cargarCuarteles();
                model.addAttribute("cuarteles", cuartelDAO.getCuarteles());
                pagina = "bajaCuartel";
                break;

            case "baja_servicio":
                servicioDAO.cargarServicios();
                model.addAttribute("servicios", servicioDAO.getServicios());
                pagina = "bajaServicio";
                break;

            case "asignar_servicio":
                servicioDAO.cargarServicios();
                militarDAO.cargarMilitares();
                model.addAttribute("militares", militarDAO.getMilitares());
                model.addAttribute("servicios", servicioDAO.getServicios());

                pagina = "asignarServicio";
                break;

            case "desasignar_servicio":
                militarDAO.cargarMilitares();
                servicioDAO.cargarServicios();
                servicioRealizadoDAO.cargarServiciosRealizados();
                model.addAttribute("militares", militarDAO.getMilitares());
                model.addAttribute("servicios", servicioDAO.getServicios());
                model.addAttribute("serviciosRealizados", servicioRealizadoDAO.getServiciosRealizados());

                pagina = "desasignarServicio";
                break;

            case "logout":
                this.militarLogeado = null;
                pagina = "index";
                break;
            default:
                model.addAttribute("error", "Opción invàlida");
                pagina = "error";
                break;

        }

        //JOptionhowMessageDialog(null, "Salgo del switch");
        return pagina;
    }

    @PostMapping("/ingresarMilitar")
    public String ingresarMilitar(@RequestParam(name = "tipo") String tipo, Model model) {
        String pagina = null;
        switch (tipo) {
            case "Oficial":
                pagina = "ingresarOficial";
                break;
            case "Suboficial":
                pagina = "ingresarSuboficial";
                break;

            case "Soldado":
                String faltante = "";
                boolean faltaElemento = false;

                companiaDAO.cargarCompanias();
                cuerpoDAO.cargarCuerpos();
                cuartelDAO.cargarCuarteles();

                if (companiaDAO.getCompanias().isEmpty()) {
                    faltaElemento = true;
                    faltante = "companias";
                } else if (cuerpoDAO.getCuerpos().isEmpty()) {
                    faltaElemento = true;
                    faltante = "cuerpos";
                } else if (cuartelDAO.getCuarteles().isEmpty()) {
                    faltaElemento = true;
                    faltante = "cuarteles";
                }
                if (!faltaElemento) {
                    model.addAttribute("companias", companiaDAO.getCompanias());
                    model.addAttribute("cuerpos", cuerpoDAO.getCuerpos());
                    model.addAttribute("cuarteles", cuartelDAO.getCuarteles());

                    pagina = "ingresarSoldado";
                } else {
                    model.addAttribute("error", "No se puede dar de alta un soldado, ya que no hay " + faltante + " disponibles");
                    pagina = "error";
                }

                break;

            default:
                model.addAttribute("error", "Opcion invalida");
                break;
        }
        return pagina;
    }

    @PostMapping("/ingresarCompania")
    public String ingresarCompania(@RequestParam(name = "codigo") String codigo,
            @RequestParam(name = "actividad") String actividad,
            Model model) {
        if (codigo.isEmpty() || actividad.isEmpty()) {
            String msgError = "Uno de los campos ingresados está vacío";
            model.addAttribute("error", msgError);
            return "error";
        }
        companiaDAO.ingresarCompania(codigo, actividad);
        return "altaExitosa";
    }

    @PostMapping("/ingresarCuartel")
    public String ingresarCuartel(@RequestParam(name = "codigo") String codigo, @RequestParam(name = "nombre") String nombre, @RequestParam(name = "ubicacion") String ubicacion, Model model) {
        if (codigo.isEmpty() || nombre.isEmpty() || ubicacion.isEmpty()) {
            String msgError = "Uno de los campos ingresados está vacío";
            model.addAttribute("error", msgError);
            return "error";
        }
        cuartelDAO.ingresarCuartel(codigo, nombre, ubicacion);
        return "altaExitosa";
    }

    @PostMapping("/ingresarCuerpo")
    public String ingresarCuerpo(@RequestParam(name = "codigo") String codigo, @RequestParam(name = "denominacion") String denominacion, Model model) {
        if (codigo.isEmpty() || denominacion.isEmpty()) {
            String msgError = "Uno de los campos ingresados está vacío";
            model.addAttribute("error", msgError);
            return "error";
        }
        cuerpoDAO.ingresarCuerpo(codigo, denominacion);
        return "altaExitosa";
    }

    @PostMapping("/ingresarServicio")
    public String ingresarServicio(@RequestParam(name = "codigo") String codigo, @RequestParam(name = "descripcion") String descripcion, Model model) {
        if (codigo.isEmpty() || descripcion.isEmpty()) {
            String msgError = "Uno de los campos ingresados está vacío";
            model.addAttribute("error", msgError);
            return "error";
        }
        servicioDAO.ingresarServicio(codigo, descripcion);
        return "altaExitosa";
    }

    @PostMapping("/ingresarOficial")
    public String ingresarOficial(@RequestParam(name = "codigo") String codigo,
            @RequestParam(name = "password") String password,
            @RequestParam(name = "nombre") String nombre,
            @RequestParam(name = "apellido") String apellido,
            @RequestParam(name = "graduacion") String graduacion,
            Model model) {
        if (codigo.isEmpty() || password.isEmpty() || nombre.isEmpty() || apellido.isEmpty() || graduacion.isEmpty()) {
            String msgError = "Uno de los campos ingresados está vacío";
            model.addAttribute("error", msgError);
            return "error";
        }
        if (militarDAO.verificarTipo(codigo, "Oficial")) {
            militarDAO.ingresarOficial(codigo, password, nombre, apellido, graduacion);
            return "altaExitosa";
        } else {
            model.addAttribute("error", "El militar ingresado existe y no corresponde a un oficial");
            return "error";
        }
    }

    @PostMapping("/ingresarSuboficial")
    public String ingresarSuboficial(@RequestParam(name = "codigo") String codigo,
            @RequestParam(name = "password") String password,
            @RequestParam(name = "nombre") String nombre,
            @RequestParam(name = "apellido") String apellido,
            @RequestParam(name = "graduacion") String graduacion,
            Model model) {
        if (codigo.isEmpty() || password.isEmpty() || nombre.isEmpty() || apellido.isEmpty() || graduacion.isEmpty()) {
            String msgError = "Uno de los campos ingresados está vacío";
            model.addAttribute("error", msgError);
            return "error";
        }
        if (militarDAO.verificarTipo(codigo, "Suboficial")) {
            militarDAO.ingresarSuboficial(codigo, password, nombre, apellido, graduacion);
            return "altaExitosa";
        } else {
            model.addAttribute("error", "El militar ingresado existe y no corresponde a un suboficial");
            return "error";
        }

    }

    @PostMapping("/ingresarSoldado")
    public String ingresarSoldado(@RequestParam(name = "codigo") String codigo,
            @RequestParam(name = "password") String password,
            @RequestParam(name = "nombre") String nombre,
            @RequestParam(name = "apellido") String apellido,
            @RequestParam(name = "graduacion") String graduacion,
            @RequestParam(name = "codCompania") String codCompania,
            @RequestParam(name = "codCuerpo") String codCuerpo,
            @RequestParam(name = "codCuartel") String codCuartel,
            Model model) {
        if (codigo.isEmpty() || password.isEmpty() || nombre.isEmpty() || apellido.isEmpty() || graduacion.isEmpty()) {
            String msgError = "Uno de los campos ingresados está vacío";
            model.addAttribute("error", msgError);
            return "error";
        }
        if (militarDAO.verificarTipo(codigo, "Soldado")) {
            militarDAO.ingresarSoldado(codigo, password, nombre, apellido, graduacion, codCompania, codCuerpo, codCuartel);
            return "altaExitosa";
        } else {
            model.addAttribute("error", "El militar ingresado existe y no corresponde a un soldado");
            return "error";
        }
    }

    @PostMapping("/bajaMilitar")
    public String bajaMilitar(@RequestParam(name = "militarBaja") String codigo, Model model) {
        String pagina = null;
        try {
            boolean cargado = false;

            servicioRealizadoDAO.cargarServiciosRealizados();

            for (ServicioRealizadoDTO sr : servicioRealizadoDAO.getServiciosRealizados()) {
                //JOptionPane.showMessageDialog(null, sr);
                if (sr.getCodSoldado().toString().equals(codigo)) {
                    //JOptionPane.showMessageDialog(null, "Entro al if");
                    cargado = true;
                }
            }

            if (!cargado) {
                militarDAO.bajaMilitar(codigo);
                pagina = "bajaExitosa";
            } else {
                model.addAttribute("error", "No se puede dar de baja el soldado, ya que el mismo tiene asignado un servicio");

                pagina = "error";
            }

        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e);
            model.addAttribute("error", e.getMessage());
            pagina = "error";
        }
        return pagina;
    }

    @PostMapping("/bajaCompania")
    public String bajaCompania(@RequestParam(name = "companiaBaja") String codigo, Model model) {
        String pagina = null;
        try {
            boolean cargado = false;
            militarDAO.cargarMilitares();

            for (MilitarDTO militar : militarDAO.getMilitares()) {
                //JOptionPane.showMessageDialog(null, "Entro al for");
                if (militar instanceof SoldadoDTO) {
                    //JOptionPane.showMessageDialog(null, militar);
                    SoldadoDTO soldado = (SoldadoDTO) militar;
                    if (soldado.getCodCompania().toString().equals(codigo)) {
                        cargado = true;
                    }
                }
            }

            if (!cargado) {
                companiaDAO.bajaCompania(codigo);
                pagina = "bajaExitosa";
            } else {
                model.addAttribute("error", "No se puede dar de baja la compania, ya que la misma está asignada a un soldado");
                pagina = "error";
            }

        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e);
            model.addAttribute("error", e.getMessage());
            pagina = "error";

        }
        return pagina;
    }

    @PostMapping("/bajaCuerpo")
    public String bajaCuerpo(@RequestParam(name = "cuerpoBaja") String codigo, Model model) {
        String pagina = null;
        try {
            boolean cargado = false;
            militarDAO.cargarMilitares();

            for (MilitarDTO militar : militarDAO.getMilitares()) {
                //JOptionPane.showMessageDialog(null, "Entro al for");
                if (militar instanceof SoldadoDTO) {
                    //JOptionPane.showMessageDialog(null, militar);
                    SoldadoDTO soldado = (SoldadoDTO) militar;
                    if (soldado.getCodCuerpo().toString().equals(codigo)) {
                        cargado = true;
                    }
                }
            }

            if (!cargado) {
                cuerpoDAO.bajaCuerpo(codigo);
                pagina = "bajaExitosa";
            } else {
                model.addAttribute("error", "No se puede dar de baja el cuerpo, ya que el mismo está asignado a un soldado");
                pagina = "error";
            }

        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e);
            model.addAttribute("error", e.getMessage());
            pagina = "error";

        }
        return pagina;
    }

    @PostMapping("/bajaCuartel")
    public String bajaCuartel(@RequestParam(name = "cuartelBaja") String codigo, Model model) {
        String pagina = null;
        try {
            boolean cargado = false;
            militarDAO.cargarMilitares();

            for (MilitarDTO militar : militarDAO.getMilitares()) {
                //JOptionPane.showMessageDialog(null, "Entro al for");
                if (militar instanceof SoldadoDTO) {
                    //JOptionPane.showMessageDialog(null, militar);
                    SoldadoDTO soldado = (SoldadoDTO) militar;
                    if (soldado.getCodCuartel().toString().equals(codigo)) {
                        cargado = true;
                    }
                }
            }

            if (!cargado) {
                cuartelDAO.bajaCuartel(codigo);
                pagina = "bajaExitosa";
            } else {
                model.addAttribute("error", "No se puede dar de baja el cuartel, ya que el mismo está asignado a un soldado");
                pagina = "error";
            }

        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e);
            model.addAttribute("error", e.getMessage());
            pagina = "error";

        }
        return pagina;
    }

    @PostMapping("/bajaServicio")
    public String bajaServicio(@RequestParam(name = "servicioBaja") String codigo, Model model) {
        String pagina = null;
        try {
            boolean cargado = false;

            servicioRealizadoDAO.cargarServiciosRealizados();

            for (ServicioRealizadoDTO sr : servicioRealizadoDAO.getServiciosRealizados()) {

                if (sr.getCodServicio().toString().equals(codigo)) {
                    cargado = true;
                }
            }

            if (!cargado) {
                servicioDAO.bajaServicio(codigo);
                pagina = "bajaExitosa";
            } else {
                model.addAttribute("error", "No se puede dar de baja el servicio, ya que está asignado a un soldado");

                pagina = "error";
            }

        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e);
            model.addAttribute("error", e.getMessage());
            pagina = "error";
        }
        return pagina;
    }

    @PostMapping("/asignarServicio")
    public String asignarServicio(@RequestParam(name = "codSoldado") String codSoldado,
            @RequestParam(name = "codServicio") String codServicio,
            @RequestParam(name = "fecha") String fecha,
            Model model) {
        servicioRealizadoDAO.asignarServicio(codSoldado, codServicio, fecha);
        return "altaExitosa";
    }

    @PostMapping("/desasignarServicio")
    public String desasignarServicio(@RequestParam(name = "servicioDesasignado") String codigo,
            Model model) {
        servicioRealizadoDAO.desasignarServicio(codigo);
        return "bajaExitosa";
    }

}
