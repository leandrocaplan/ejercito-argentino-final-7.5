/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import DTO.ServicioDTO;
import controlador.*;
import java.sql.SQLException;
import java.util.ArrayList;
import javax.swing.JOptionPane;
import DTO.MilitarDTO;
import org.apache.log4j.BasicConfigurator;
import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.stereotype.Component;

/**
 *
 * @author Leandro
 */
@Component
public class ServicioDAO {

    private ArrayList<ServicioDTO> servicios;

    public void cargarServicios() {
        try {
//            ////JOptionhowMessageDialog(null, "Entro a cargarMilitares");
            this.servicios = new ArrayList<ServicioDTO>();

            // ////JOptionhowMessageDialog(null, query);
            BasicConfigurator.configure();
            Session session = HibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            Query query = session.createQuery("from ServicioDTO");
            this.servicios = (ArrayList) query.list();
            //JOptionPane.showMessageDialog(null, this.servicios);
            session.getTransaction().commit();
            session.close();
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e);
        }
    }

    public void ingresarServicio(String codigo, String descripcion) {
        try {

            BasicConfigurator.configure();
            Session session = HibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            ServicioDTO nuevoServicio = new ServicioDTO(Integer.parseInt(codigo), descripcion);
            //JOptionPane.showMessageDialog(null, nuevoServicio);
            session.saveOrUpdate(nuevoServicio);
            //JOptionPane.showMessageDialog(null, this.servicios);

            session.getTransaction().commit();
            session.close();
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e);
        }
    }

    public void bajaServicio(String codigo) {
        BasicConfigurator.configure();
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();
        try {

            String hql = "delete from ServicioDTO where codigo=:codigo";
            Query query = session.createQuery(hql);
            query.setString("codigo", codigo);
            //JOptionPane.showMessageDialog(null, query.executeUpdate());
            query.executeUpdate();
            session.getTransaction().commit();
            session.close();
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e);
        }
    }

    public ServicioDAO(ArrayList<ServicioDTO> servicios) {
        this.servicios = servicios;
    }

    public ServicioDAO() {
    }

    public ArrayList<ServicioDTO> getServicios() {
        return servicios;
    }

    public void setServicios(ArrayList<ServicioDTO> servicios) {
        this.servicios = servicios;
    }

}
