/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import DTO.MilitarDTO;
import controlador.*;
import java.sql.SQLException;
import java.util.ArrayList;
import javax.swing.JOptionPane;
import DTO.OficialDTO;
import DTO.SoldadoDTO;
import DTO.SuboficialDTO;
import org.apache.log4j.BasicConfigurator;
import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.stereotype.Component;

/**
 *
 * @author Leandro
 */
@Component
public class MilitarDAO {

    private ArrayList<MilitarDTO> militares;
    private ArrayList<SoldadoDTO> soldados;
    
    public MilitarDTO login(String cod) {
        MilitarDTO militar = null;
        //JOptionPane.showMessageDialog(null, "Entro a login");
        //JOptionPane.showMessageDialog(null, "Codigo:" + cod);
        BasicConfigurator.configure();
        Session session = HibernateUtil.getSessionFactory().openSession();
        //JOptionPane.showMessageDialog(null, session);
        session.beginTransaction();
        Query query = session.createQuery("from MilitarDTO m where m.codigo=:cod");
        //JOptionPane.showMessageDialog(null, query);

        query.setInteger("cod", Integer.parseInt(cod));
        //Query query = session.createQuery("from MilitarDTO");
        //JOptionPane.showMessageDialog(null, query);
        //JOptionPane.showMessageDialog(null, query.list());
        if (!query.list().isEmpty()) {
            militar = (MilitarDTO) query.list().get(0);
        }
        //JOptionPane.showMessageDialog(null, militar);
        session.getTransaction().commit();
        session.close();

        return militar;
    }

    public void cargarMilitares() {
        try {
//            ////JOptionhowMessageDialog(null, "Entro a cargarMilitares");
            this.militares = new ArrayList<MilitarDTO>();
            ArrayList<SoldadoDTO> soldados = new ArrayList<SoldadoDTO>();
            ArrayList<SuboficialDTO> suboficiales = new ArrayList<SuboficialDTO>();
            ArrayList<OficialDTO> oficiales = new ArrayList<OficialDTO>();
            // ////JOptionhowMessageDialog(null, query);
            BasicConfigurator.configure();
            Session session = HibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();

            Query query = session.createQuery("from SoldadoDTO");
            soldados = (ArrayList) query.list();

            query = session.createQuery("from SuboficialDTO");
            suboficiales = (ArrayList) query.list();

            query = session.createQuery("from OficialDTO");
            oficiales = (ArrayList) query.list();

            this.militares.addAll(oficiales);
            this.militares.addAll(suboficiales);
            this.militares.addAll(soldados);

            //JOptionPane.showMessageDialog(null, this.militares);
            session.getTransaction().commit();
            session.close();
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e);
        }
    }

    public void cargarSoldados() {
        try {
//            ////JOptionhowMessageDialog(null, "Entro a cargarMilitares");
            // ////JOptionhowMessageDialog(null, query);
            BasicConfigurator.configure();
            Session session = HibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();

            Query query = session.createQuery("from SoldadoDTO");
            this.soldados = (ArrayList) query.list();

            //JOptionPane.showMessageDialog(null, this.militares);
            session.getTransaction().commit();
            session.close();
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e);
        }
    }
    public void ingresarOficial(String codigo, String password, String nombre, String apellido, String graduacion) {
        try {

            BasicConfigurator.configure();
            Session session = HibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            OficialDTO nuevoOficial = new OficialDTO(Integer.parseInt(codigo), "Oficial", password, nombre, apellido, graduacion);
            //JOptionPane.showMessageDialog(null, nuevoOficial);
            session.saveOrUpdate(nuevoOficial);
            //JOptionPane.showMessageDialog(null, this.militares);

            session.getTransaction().commit();
            session.close();
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e);
        }
    }

    public void ingresarSuboficial(String codigo, String password, String nombre, String apellido, String graduacion) {
        try {

            BasicConfigurator.configure();
            Session session = HibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            SuboficialDTO nuevoSuboficial = new SuboficialDTO(Integer.parseInt(codigo), "Suboficial", password, nombre, apellido, graduacion);
            //JOptionPane.showMessageDialog(null, nuevoSuboficial);
            session.saveOrUpdate(nuevoSuboficial);
            //JOptionPane.showMessageDialog(null, this.militares);

            session.getTransaction().commit();
            session.close();
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e);
        }
    }

    public void ingresarSoldado(String codigo,
            String password,
            String nombre,
            String apellido,
            String graduacion,
            String codCompania,
            String codCuerpo,
            String codCuartel) {
        try {

            BasicConfigurator.configure();
            Session session = HibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            SoldadoDTO nuevoSoldado = new SoldadoDTO(
                    Integer.parseInt(codigo),
                    "Soldado",
                    password,
                    nombre,
                    apellido,
                    graduacion,
                    Integer.parseInt(codCompania),
                    Integer.parseInt(codCuerpo),
                    Integer.parseInt(codCuartel));
            //JOptionPane.showMessageDialog(null, nuevoSoldado);
            session.saveOrUpdate(nuevoSoldado);
            //JOptionPane.showMessageDialog(null, this.militares);

            session.getTransaction().commit();
            session.close();
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e);
        }
    }

    public void bajaMilitar(String codigo) {
        BasicConfigurator.configure();
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();
        try {

            String hql = "delete from MilitarDTO where codigo=:codigo";
            Query query = session.createQuery(hql);
            query.setString("codigo", codigo);
            //JOptionPane.showMessageDialog(null,query.executeUpdate());
            query.executeUpdate();
            session.getTransaction().commit();
            session.close();
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e);
        }
    }

    public boolean verificarTipo(String cod, String tipo) {
        MilitarDTO militar = null;
        boolean tipoCorrecto = false;
        //JOptionPane.showMessageDialog(null, "Entro a login");
        //JOptionPane.showMessageDialog(null, "Codigo:" + cod);
        BasicConfigurator.configure();
        Session session = HibernateUtil.getSessionFactory().openSession();
        //JOptionPane.showMessageDialog(null, session);
        session.beginTransaction();
        Query query = session.createQuery("from MilitarDTO m where m.codigo=:cod");
        //JOptionPane.showMessageDialog(null, query);

        query.setInteger("cod", Integer.parseInt(cod));
        //Query query = session.createQuery("from MilitarDTO");
        //JOptionPane.showMessageDialog(null, query);
        //JOptionPane.showMessageDialog(null, query.list());
        if (!query.list().isEmpty()) {
            militar = (MilitarDTO) query.list().get(0);
        }
        //JOptionPane.showMessageDialog(null, militar);
        if (militar == null) {
            tipoCorrecto = true;
        } //JOptionPane.showMessageDialog(null, militar);
        else if (militar.getTipo().equals(tipo)) {
            tipoCorrecto = true;
        }
        session.getTransaction().commit();
        session.close();

        return tipoCorrecto;
    }

    public ArrayList<MilitarDTO> getMilitares() {
        return militares;
    }

    public void setMilitares(ArrayList<MilitarDTO> militares) {
        this.militares = militares;
    }

    public ArrayList<SoldadoDTO> getSoldados() {
        return soldados;
    }

    public void setSoldados(ArrayList<SoldadoDTO> soldados) {
        this.soldados = soldados;
    }

}
