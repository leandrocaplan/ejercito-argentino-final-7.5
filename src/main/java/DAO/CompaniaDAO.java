/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import DTO.CompaniaDTO;
import controlador.*;
import java.sql.SQLException;
import java.util.ArrayList;
import javax.swing.JOptionPane;
import DTO.MilitarDTO;
import org.apache.log4j.BasicConfigurator;
import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.stereotype.Component;

/**
 *
 * @author Leandro
 */
@Component
public class CompaniaDAO {

    private ArrayList<CompaniaDTO> companias;

    public void cargarCompanias() {
        try {
//            ////JOptionhowMessageDialog(null, "Entro a cargarMilitares");
            this.companias = new ArrayList<CompaniaDTO>();

            // ////JOptionhowMessageDialog(null, query);
            BasicConfigurator.configure();
            Session session = HibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            Query query = session.createQuery("from CompaniaDTO");
            this.companias = (ArrayList) query.list();
            //JOptionPane.showMessageDialog(null, this.companias);
            session.getTransaction().commit();
            session.close();
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e);
        }
    }

    public void ingresarCompania(String codigo, String actividad) {
        try {

            BasicConfigurator.configure();
            Session session = HibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            CompaniaDTO nuevaCompania = new CompaniaDTO(Integer.parseInt(codigo), actividad);
            //JOptionPane.showMessageDialog(null, nuevaCompania);
            session.saveOrUpdate(nuevaCompania);
            //JOptionPane.showMessageDialog(null, this.companias);

            session.getTransaction().commit();
            session.close();
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e);
        }
    }
    
    public void bajaCompania(String codigo) {
        BasicConfigurator.configure();
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();
        try {

            String hql = "delete from CompaniaDTO where codigo=:codigo";
            Query query = session.createQuery(hql);
            query.setString("codigo", codigo);
            //JOptionPane.showMessageDialog(null,query.executeUpdate());
            query.executeUpdate();
            session.getTransaction().commit();
            session.close();
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e);
        }
    }

    public CompaniaDAO(ArrayList<CompaniaDTO> companias) {
        this.companias = companias;
    }

    public CompaniaDAO() {
    }

    public ArrayList<CompaniaDTO> getCompanias() {
        return companias;
    }

    public void setCompanias(ArrayList<CompaniaDTO> companias) {
        this.companias = companias;
    }

}
