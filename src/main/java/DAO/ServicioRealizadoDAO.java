/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import DTO.ServicioRealizadoDTO;
import controlador.*;
import static java.lang.Math.random;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Random;
import javax.swing.JOptionPane;
import DTO.MilitarDTO;
import org.apache.log4j.BasicConfigurator;
import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.stereotype.Component;

/**
 *
 * @author Leandro
 */
@Component
public class ServicioRealizadoDAO {

    private ArrayList<ServicioRealizadoDTO> serviciosRealizados;

    public void cargarServiciosRealizados() {
        try {
//            ////JOptionhowMessageDialog(null, "Entro a cargarMilitares");
            this.serviciosRealizados = new ArrayList<ServicioRealizadoDTO>();

            // ////JOptionhowMessageDialog(null, query);
            BasicConfigurator.configure();
            Session session = HibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            Query query = session.createQuery("from ServicioRealizadoDTO");
            this.serviciosRealizados = (ArrayList) query.list();
            //JOptionPane.showMessageDialog(null, this.serviciosRealizados);
            session.getTransaction().commit();
            session.close();
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e);
        }
    }

    public void asignarServicio(String codSoldado, String codServicio, String fecha) {
        try {

            BasicConfigurator.configure();
            Session session = HibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();

            ServicioRealizadoDTO nuevoServicioRealizado = new ServicioRealizadoDTO(null,
                    Integer.parseInt(codSoldado),
                    Integer.parseInt(codServicio), 
                    fecha);
            //JOptionPane.showMessageDialog(null, nuevoServicioRealizado);
            session.saveOrUpdate(nuevoServicioRealizado);
            //JOptionPane.showMessageDialog(null, this.serviciosRealizados);

            session.getTransaction().commit();
            session.close();
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e);
        }
    }
    public void desasignarServicio(String codigo) {
        BasicConfigurator.configure();
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();
        try {

            String hql = "delete from ServicioRealizadoDTO where codServicioRealizado=:codigo";
            Query query = session.createQuery(hql);
            query.setString("codigo", codigo);
            //JOptionPane.showMessageDialog(null, query.executeUpdate());
            query.executeUpdate();
            session.getTransaction().commit();
            session.close();
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e);
        }
    }

    public ServicioRealizadoDAO(ArrayList<ServicioRealizadoDTO> serviciosRealizados) {
        this.serviciosRealizados = serviciosRealizados;
    }

    public ServicioRealizadoDAO() {
    }

    public ArrayList<ServicioRealizadoDTO> getServiciosRealizados() {
        return serviciosRealizados;
    }

    public void setServiciosRealizados(ArrayList<ServicioRealizadoDTO> serviciosRealizados) {
        this.serviciosRealizados = serviciosRealizados;
    }

}
