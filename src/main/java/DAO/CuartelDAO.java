/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import DTO.CuartelDTO;
import controlador.*;
import java.sql.SQLException;
import java.util.ArrayList;
import javax.swing.JOptionPane;
import DTO.MilitarDTO;
import org.apache.log4j.BasicConfigurator;
import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.stereotype.Component;

/**
 *
 * @author Leandro
 */
@Component
public class CuartelDAO {

    private ArrayList<CuartelDTO> cuarteles;

    public void cargarCuarteles() {

//            ////JOptionhowMessageDialog(null, "Entro a cargarMilitares");
        this.cuarteles = new ArrayList<CuartelDTO>();

        // ////JOptionhowMessageDialog(null, query);
        BasicConfigurator.configure();
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();
        Query query = session.createQuery("from CuartelDTO");
        this.cuarteles = (ArrayList) query.list();
        session.getTransaction().commit();
        session.close();
    }

    public void ingresarCuartel(String codigo, String nombre, String ubicacion) {
        try {

            BasicConfigurator.configure();
            Session session = HibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            CuartelDTO nuevoCuartel = new CuartelDTO(Integer.parseInt(codigo), nombre, ubicacion);
            //JOptionPane.showMessageDialog(null, nuevoCuartel);
            session.saveOrUpdate(nuevoCuartel);
            //JOptionPane.showMessageDialog(null, this.cuarteles);

            session.getTransaction().commit();
            session.close();
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e);
        }
    }

    public void bajaCuartel(String codigo) {
        BasicConfigurator.configure();
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();
        try {

            String hql = "delete from CuartelDTO where codigo=:codigo";
            Query query = session.createQuery(hql);
            query.setString("codigo", codigo);
            //JOptionPane.showMessageDialog(null, query.executeUpdate());
            query.executeUpdate();
            session.getTransaction().commit();
            session.close();
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e);
        }
    }

    public CuartelDAO() {
    }

    public CuartelDAO(ArrayList<CuartelDTO> cuarteles) {
        this.cuarteles = cuarteles;
    }

    public ArrayList<CuartelDTO> getCuarteles() {
        return cuarteles;
    }

    public void setCuarteles(ArrayList<CuartelDTO> cuarteles) {
        this.cuarteles = cuarteles;
    }

}
