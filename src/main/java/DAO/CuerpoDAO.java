/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import DTO.CuerpoDTO;
import controlador.*;
import java.sql.SQLException;
import java.util.ArrayList;
import javax.swing.JOptionPane;

import org.apache.log4j.BasicConfigurator;
import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.stereotype.Component;

/**
 *
 * @author Leandro
 */
@Component
public class CuerpoDAO {

    private ArrayList<CuerpoDTO> cuerpos;

    public void cargarCuerpos() {

//            ////JOptionhowMessageDialog(null, "Entro a cargarMilitares");
        this.cuerpos = new ArrayList<CuerpoDTO>();

        // ////JOptionhowMessageDialog(null, query);
        BasicConfigurator.configure();
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();
        Query query = session.createQuery("from CuerpoDTO");
        this.cuerpos = (ArrayList) query.list();
        session.getTransaction().commit();
        session.close();
    }

    public void ingresarCuerpo(String codigo, String denominacion) {
        try {

            BasicConfigurator.configure();
            Session session = HibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            CuerpoDTO nuevoCuerpo = new CuerpoDTO(Integer.parseInt(codigo), denominacion);
            //JOptionPane.showMessageDialog(null, nuevoCuerpo);
            session.saveOrUpdate(nuevoCuerpo);
            //JOptionPane.showMessageDialog(null, this.cuerpos);

            session.getTransaction().commit();
            session.close();
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e);
        }
    }

    public void bajaCuerpo(String codigo) {
        BasicConfigurator.configure();
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();
        try {

            String hql = "delete from CuerpoDTO where codigo=:codigo";
            Query query = session.createQuery(hql);
            query.setString("codigo", codigo);
            //JOptionPane.showMessageDialog(null, query.executeUpdate());
            query.executeUpdate();
            session.getTransaction().commit();
            session.close();
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e);
        }
    }

    public CuerpoDAO() {
    }

    public CuerpoDAO(ArrayList<CuerpoDTO> cuerpos) {
        this.cuerpos = cuerpos;
    }

    public ArrayList<CuerpoDTO> getCuerpos() {
        return cuerpos;
    }

    public void setCuerpos(ArrayList<CuerpoDTO> cuerpos) {
        this.cuerpos = cuerpos;
    }

}
