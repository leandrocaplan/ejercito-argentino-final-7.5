-- phpMyAdmin SQL Dump
-- version 4.6.6
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost
-- Tiempo de generación: 08-03-2020 a las 06:35:49
-- Versión del servidor: 5.7.17-log
-- Versión de PHP: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `parcial`
--
CREATE DATABASE IF NOT EXISTS `ejercito` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `ejercito`;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `companias`
--

CREATE TABLE `companias` (
  `Codigo` int(11) NOT NULL,
  `Actividad` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `companias`
--

INSERT INTO `companias` (`Codigo`, `Actividad`) VALUES
(3, 'Otra'),
(13, 'Papa'),
(44, 'Debugear'),
(311, 'Alguna'),
(888, 'Codificar'),
(6666, 'Actualizo');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cuarteles`
--

CREATE TABLE `cuarteles` (
  `Codigo` int(11) NOT NULL,
  `Nombre` varchar(50) NOT NULL,
  `Ubicacion` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `cuarteles`
--

INSERT INTO `cuarteles` (`Codigo`, `Nombre`, `Ubicacion`) VALUES
(88, 'Patricios', 'Palermo'),
(98, 'Modificado', 'San Miguel'),
(134, 'Puerta 8', 'Podestá'),
(666, 'Campo de Mayo', 'San Miguel'),
(5538, 'Otro', 'Nuñez');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cuerpos`
--

CREATE TABLE `cuerpos` (
  `Codigo` int(11) NOT NULL,
  `Denominacion` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `cuerpos`
--

INSERT INTO `cuerpos` (`Codigo`, `Denominacion`) VALUES
(343, 'Saraza'),
(422, 'Hola'),
(555, 'Beta'),
(3433, 'Artilleria'),
(5353, 'Cuerpo');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `militares`
--

CREATE TABLE `militares` (
  `Codigo` int(11) NOT NULL,
  `Password` varchar(50) NOT NULL,
  `Tipo` varchar(10) NOT NULL,
  `Nombre` varchar(50) NOT NULL,
  `Apellido` varchar(50) NOT NULL,
  `Graduacion` varchar(50) NOT NULL,
  `Compania` int(11) DEFAULT NULL,
  `Cuerpo` int(11) DEFAULT NULL,
  `Cuartel` int(11) DEFAULT NULL,
  `ServiciosRealizados` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `militares`
--

INSERT INTO `militares` (`Codigo`, `Password`, `Tipo`, `Nombre`, `Apellido`, `Graduacion`, `Compania`, `Cuerpo`, `Cuartel`, `ServiciosRealizados`) VALUES
(55, 'aaa', 'Soldado', 'Matias', 'Lopez', 'Voluntario de primera', 44, 343, 88, NULL),
(90, 'zzz', 'Soldado', 'Guillermo', 'Lopez', 'Voluntario de primera', 3, 343, 88, NULL),
(664, 'qqq', 'Suboficial', 'Pablo', 'Nuñez', 'Sargento Ayudante', NULL, NULL, NULL, NULL),
(665, 'qqq', 'Soldado', 'Matias', 'Avalos', 'Voluntario de segunda', 888, 555, 666, NULL),
(666, 'qqq', 'Soldado', 'Marcela', 'Paoli', 'Voluntario de primera', 3, 343, 88, NULL),
(667, 'qqq', 'Suboficial', 'Juana', 'de Arco', 'Suboficial Mayor', NULL, NULL, NULL, NULL),
(754, 'ooo', 'Oficial', 'Maria', 'Antonieta', 'Teniente general', NULL, NULL, NULL, NULL),
(777, 'aaa', 'Oficial', 'Jorge', 'Lopez', 'Teniente general', NULL, NULL, NULL, NULL),
(999, 'aaa', 'Soldado', 'Pepe', 'Luis', 'Voluntario de primera', 3, 343, 88, NULL),
(1000, 'eee', 'Soldado', 'Mauricio', 'Catan', 'Voluntario de primera', 3, 343, 88, NULL),
(1222, 'zzz', 'Oficial', 'Luigi', 'Mario', 'Teniente general', NULL, NULL, NULL, NULL),
(1234, 'qwerty', 'Oficial', 'Lorenzo', 'Miguel', 'Mayor', NULL, NULL, NULL, NULL),
(3121, '222', 'Suboficial', 'Maria', 'Gomez', 'Suboficial Mayor', NULL, NULL, NULL, NULL),
(4321, 'zzz', 'Suboficial', 'Miguel', 'Angel', 'Suboficial Mayor', NULL, NULL, NULL, NULL),
(8989, 'ttt', 'Oficial', 'Flavia', 'Palmeiro', 'Teniente general', NULL, NULL, NULL, NULL),
(9090, 'yyy', 'Suboficial', 'Italo', 'Luder', 'Suboficial Mayor', NULL, NULL, NULL, NULL),
(9900, 'qqq', 'Oficial', 'Antonio', 'Gimenez', 'Teniente general', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `servicios`
--

CREATE TABLE `servicios` (
  `Codigo` int(11) NOT NULL,
  `Descripcion` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `servicios`
--

INSERT INTO `servicios` (`Codigo`, `Descripcion`) VALUES
(11, 'escribiente'),
(66, 'Inspeccionar'),
(89, 'Exponer'),
(232, 'Correr'),
(455, 'Lagartijas'),
(996, 'Barrer'),
(8989, 'Saltar');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `serviciosrealizados`
--

CREATE TABLE `serviciosrealizados` (
  `CodigoServicioRealizado` int(11) NOT NULL,
  `CodigoSoldado` int(11) NOT NULL,
  `CodigoServicio` int(11) NOT NULL,
  `Fecha` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `serviciosrealizados`
--

INSERT INTO `serviciosrealizados` (`CodigoServicioRealizado`, `CodigoSoldado`, `CodigoServicio`, `Fecha`) VALUES
(21, 665, 455, '2019-11-04'),
(26, 665, 89, '2019-11-08'),
(27, 90, 89, '2019-04-21'),
(29, 55, 11, '1992-02-21'),
(30, 1000, 66, '2020-03-13');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `companias`
--
ALTER TABLE `companias`
  ADD PRIMARY KEY (`Codigo`);

--
-- Indices de la tabla `cuarteles`
--
ALTER TABLE `cuarteles`
  ADD PRIMARY KEY (`Codigo`);

--
-- Indices de la tabla `cuerpos`
--
ALTER TABLE `cuerpos`
  ADD PRIMARY KEY (`Codigo`);

--
-- Indices de la tabla `militares`
--
ALTER TABLE `militares`
  ADD PRIMARY KEY (`Codigo`);

--
-- Indices de la tabla `servicios`
--
ALTER TABLE `servicios`
  ADD PRIMARY KEY (`Codigo`);

--
-- Indices de la tabla `serviciosrealizados`
--
ALTER TABLE `serviciosrealizados`
  ADD PRIMARY KEY (`CodigoServicioRealizado`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `serviciosrealizados`
--
ALTER TABLE `serviciosrealizados`
  MODIFY `CodigoServicioRealizado` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
